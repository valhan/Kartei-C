#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>


#define FILENAME "questions.csv"
#define FALSE 0
#define TRUE 1

#define sLEN 1024
#define VARIETAET 2

void getfield(FILE *oFile,char* sString, int line, long num)
{


    char ch;
    int l = 0, n = 0;

    char b = FALSE;
    fseek(oFile,0,SEEK_SET);

    while( ( ch = fgetc(oFile) ) != EOF )
    {
        if(b == FALSE)
        {
            if(ch == '\n')
            {
                l++;
                n = 0;
            }
            else if(ch == ',')
                n++;
            else if(ch == '"')
                b = TRUE;

        }
        else
        {
            if( ch == '"')
                b = FALSE;
            else if(l == line && n == num)
                sprintf(sString,"%s%c",sString,ch);
        }
    }

    ch = '\0';
    strcat(sString,&ch);

}

int getValue(FILE *oFile, int line)
{
    char sNum[16];

    strcpy(sNum, "");

    getfield(oFile, sNum,line, 2);
    return atoi(sNum);
}

void incValue(FILE *oFile, int line)
{
    int i = getValue(oFile,line);


    char ch;
    int l = 0, n = 0;
    char sNum[4];

    char b = FALSE;
    fseek(oFile,0,SEEK_SET);

    while( ( ch = fgetc(oFile) ) != EOF )
    {
        if(b == FALSE)
        {
            if(ch == '\n')
            {
                l++;
                n = 0;
            }
            else if(ch == ',')
                n++;
            else if(ch == '"')
                b = TRUE;

        }
        else
        {
            if( ch == '"')
                b = FALSE;
            else if(l == line && n == 2)
            {
                break;;
            }
        }
    }

    sprintf(sNum,"%04d",i+1);
    //printf("%s\n",sNum);
    fseek(oFile,-1,SEEK_CUR);
    fwrite(sNum,sizeof(char),4,oFile);
    fflush(oFile);
}

int getFileLEN(FILE *oFile)
{
    int i = 0;
    char ch;
    char b = FALSE;

    fseek(oFile,0,SEEK_SET);


    while( ( ch = fgetc(oFile) ) != EOF )
    {
        if(b == FALSE)
        {
            if(ch == '\n')
            {
                i++;
            }
            else if(ch == '"')
                b = TRUE;

        }
        else
        {
            if( ch == '"')
                b = FALSE;
        }
    }

    //printf("%d\n", i);

    return i;
}

int openFile(FILE** oFile, char* sPath)
{
    int iLEN;
    if(*oFile != 0)
        fclose(*oFile);
    *oFile = 0;

    *oFile = fopen(sPath,"rw+");

    if(*oFile != 0)
        printf("opened new file\n");
    else
        return -1;

    iLEN = getFileLEN(*oFile);

    if(iLEN <= 0)
    {
        fprintf(stderr,"file corrupt\n");
        exit(-1);
    }

    return iLEN;
}


int main()
{


    char ch = '0';
    int iQuest;
    int iLEN;
    int i;
    int itemp, iMin;



    char sFileName[sLEN];
    char sString[sLEN];
    FILE* oFile = 0;


    strcpy(sFileName, FILENAME);
    iLEN = openFile(&oFile, sFileName);

    strcpy(sString, "");



    srand(time(NULL));

    if( iLEN == 0) return -1;

    while(1)
    {


        //genearte random number
        itemp = iQuest;
        iQuest = rand()%(iLEN);

        if(iQuest == itemp)
            iQuest = (iQuest + 1)%iLEN;

        //find Minimum
        iMin = getValue(oFile,0);
        for(i = 1; i < iLEN; i++)
        {
            itemp = getValue(oFile, i);
            if(itemp < iMin)
                iMin = itemp;
        }

        //does it fit in range?

        if(getValue(oFile,iQuest) >= iMin +VARIETAET)
            continue;

        //Question
        strcpy(sString, "");
        getfield(oFile, sString, iQuest, 0);
        system("clear");
        printf("Q:\n%s\n", sString);

        //Answer
        strcpy(sString, "");
        ch = getchar();


        getfield(oFile, sString, iQuest, 1);
        printf("A:(%d)\n%s\n\n-\n",getValue(oFile,iQuest), sString);


        //increment number
        incValue(oFile, iQuest);



        switch(ch)
        {
        case 'r':
            printf("Enter file name:\n");
            scanf("%s",sFileName);

            iLEN = openFile(&oFile, sFileName);

            break;
        case 'o':
            sprintf(sString, "libreoffice %s &", sFileName);
            printf("command: %s\n", sString);
            system(sString);
            break;
        case 'q': return 0;
        default: break;
        }


        while ((ch = getchar()) != '\n' && ch != EOF);

    }

    fclose(oFile);
}

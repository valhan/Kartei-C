README


question.csv is loaded on start up



COMPILE and run:
    gcc -o main main.c
    ./main




VALID Files:

    .csv    with 

    libre office:   save as -> edit filter settings -> 
        field delimiter = ,
        Text delimiter = "
        save cell content as shown  = TRUE
        Save cell formulae insead of calculated values = FALSE
    ->  !Quote all text cells = TRUE!
        Fixed column width = FLASE

    example lines:
    "question1","answer1","0000"
    "question2","answer2","0000"

    last coloumn indicates the amount of calls for that specific question
    read and write permissions are neccessary for the opend file

COMMANDS:
    q       quit -> close program 
    r       load file
    o       show file in libre office


programmed in c, tested under Ubuntu 16.04
e1325081@student.tuwien.ac.at
